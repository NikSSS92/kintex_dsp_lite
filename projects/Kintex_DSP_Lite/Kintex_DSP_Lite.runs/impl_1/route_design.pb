
Q
Command: %s
53*	vivadotcl2 
route_design2default:defaultZ4-113h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2"
Implementation2default:default2
xc7k325t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2"
Implementation2default:default2
xc7k325t2default:defaultZ17-349h px� 
p
,Running DRC as a precondition to command %s
22*	vivadotcl2 
route_design2default:defaultZ4-22h px� 
P
Running DRC with %s threads
24*drc2
22default:defaultZ23-27h px� 
�
�Clock Placer Checks: Sub-optimal placement for a clock-capable IO pin and MMCM pair. 
Resolution: A dedicated routing path between the two can be used if: (a) The clock-capable IO (CCIO) is placed on a CCIO capable site (b) The MMCM is placed in the same clock region as the CCIO pin. If the IOB is driving multiple MMCMs, all MMCMs must be placed in the same clock region, one clock region above or one clock region below the IOB. Both the above conditions must be met at the same time, else it may lead to longer and less predictable clock insertion delays.
 This is normally an ERROR but the CLOCK_DEDICATED_ROUTE constraint is set to FALSE allowing your design to continue. The use of this override is highly discouraged as it may lead to very poor timing results. It is recommended that this error condition be corrected in the design.

	%s (IBUF.O) is locked to %s
	%s (MMCME2_ADV.CLKIN1) is provisionally placed by clockplacer on %s
%s*DRC2�
 "�
�design_1_i/AXI_Peripheral/AXI_C2C/inst/master_fpga_gen.axi_chip2chip_master_phy_inst/master_sio_phy.axi_chip2chip_sio_input_inst/axi_chip2chip_clk_gen_inst/single_end_clk_gen.ibufg_clk_inst	�design_1_i/AXI_Peripheral/AXI_C2C/inst/master_fpga_gen.axi_chip2chip_master_phy_inst/master_sio_phy.axi_chip2chip_sio_input_inst/axi_chip2chip_clk_gen_inst/single_end_clk_gen.ibufg_clk_inst2default:default2default:default2B
 ",

IOB_X0Y123

IOB_X0Y1232default:default2default:default2�
 "�
�design_1_i/AXI_Peripheral/AXI_C2C/inst/master_fpga_gen.axi_chip2chip_master_phy_inst/master_sio_phy.axi_chip2chip_sio_input_inst/axi_chip2chip_clk_gen_inst/gen_mmcme2.mmcm_adv_inst	�design_1_i/AXI_Peripheral/AXI_C2C/inst/master_fpga_gen.axi_chip2chip_master_phy_inst/master_sio_phy.axi_chip2chip_sio_input_inst/axi_chip2chip_clk_gen_inst/gen_mmcme2.mmcm_adv_inst2default:default2default:default2L
 "6
MMCME2_ADV_X1Y2
MMCME2_ADV_X1Y22default:default2default:default2;
 #DRC|Implementation|Placement|Clocks2default:default8ZPLCK-23h px� 
�
�Placement Constraints Check for IO constraints: Invalid constraint on register %s. It has the property IOB=TRUE, but it is not driving or driven by any IO element.%s*DRC2�
 "�
Edesign_1_i/SPI_MOD/axi_spi/U0/NO_DUAL_QUAD_MODE.QSPI_NORMAL/IO1_I_REG	Edesign_1_i/SPI_MOD/axi_spi/U0/NO_DUAL_QUAD_MODE.QSPI_NORMAL/IO1_I_REG2default:default2default:default28
  DRC|Implementation|Placement|IOs2default:default8ZPLIO-6h px� 
b
DRC finished with %s
79*	vivadotcl2(
0 Errors, 2 Warnings2default:defaultZ4-198h px� 
e
BPlease refer to the DRC report (report_drc) for more information.
80*	vivadotclZ4-199h px� 
V

Starting %s Task
103*constraints2
Routing2default:defaultZ18-103h px� 
}
BMultithreading enabled for route_design using a maximum of %s CPUs17*	routeflow2
22default:defaultZ35-254h px� 
p

Phase %s%s
101*constraints2
1 2default:default2#
Build RT Design2default:defaultZ18-101h px� 
B
-Phase 1 Build RT Design | Checksum: 53905c93
*commonh px� 
�

%s
*constraints2p
\Time (s): cpu = 00:00:42 ; elapsed = 00:00:27 . Memory (MB): peak = 2064.262 ; gain = 94.3982default:defaulth px� 
v

Phase %s%s
101*constraints2
2 2default:default2)
Router Initialization2default:defaultZ18-101h px� 
o

Phase %s%s
101*constraints2
2.1 2default:default2 
Create Timer2default:defaultZ18-101h px� 
A
,Phase 2.1 Create Timer | Checksum: 53905c93
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:00:43 ; elapsed = 00:00:28 . Memory (MB): peak = 2090.820 ; gain = 120.9572default:defaulth px� 
{

Phase %s%s
101*constraints2
2.2 2default:default2,
Fix Topology Constraints2default:defaultZ18-101h px� 
M
8Phase 2.2 Fix Topology Constraints | Checksum: 53905c93
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:00:43 ; elapsed = 00:00:28 . Memory (MB): peak = 2098.195 ; gain = 128.3322default:defaulth px� 
t

Phase %s%s
101*constraints2
2.3 2default:default2%
Pre Route Cleanup2default:defaultZ18-101h px� 
F
1Phase 2.3 Pre Route Cleanup | Checksum: 53905c93
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:00:43 ; elapsed = 00:00:28 . Memory (MB): peak = 2098.195 ; gain = 128.3322default:defaulth px� 
p

Phase %s%s
101*constraints2
2.4 2default:default2!
Update Timing2default:defaultZ18-101h px� 
C
.Phase 2.4 Update Timing | Checksum: 1ceb3bb69
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:00:56 ; elapsed = 00:00:37 . Memory (MB): peak = 2167.012 ; gain = 197.1482default:defaulth px� 
�
Intermediate Timing Summary %s164*route2K
7| WNS=0.343  | TNS=0.000  | WHS=-0.401 | THS=-875.255|
2default:defaultZ35-416h px� 
}

Phase %s%s
101*constraints2
2.5 2default:default2.
Update Timing for Bus Skew2default:defaultZ18-101h px� 
r

Phase %s%s
101*constraints2
2.5.1 2default:default2!
Update Timing2default:defaultZ18-101h px� 
E
0Phase 2.5.1 Update Timing | Checksum: 2af6b5d96
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:04 ; elapsed = 00:00:42 . Memory (MB): peak = 2167.012 ; gain = 197.1482default:defaulth px� 
�
Intermediate Timing Summary %s164*route2J
6| WNS=0.343  | TNS=0.000  | WHS=N/A    | THS=N/A    |
2default:defaultZ35-416h px� 
P
;Phase 2.5 Update Timing for Bus Skew | Checksum: 1d3ecf669
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:04 ; elapsed = 00:00:42 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
I
4Phase 2 Router Initialization | Checksum: 1fda93507
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:04 ; elapsed = 00:00:42 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
p

Phase %s%s
101*constraints2
3 2default:default2#
Initial Routing2default:defaultZ18-101h px� 
C
.Phase 3 Initial Routing | Checksum: 203287fcb
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:09 ; elapsed = 00:00:44 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
s

Phase %s%s
101*constraints2
4 2default:default2&
Rip-up And Reroute2default:defaultZ18-101h px� 
u

Phase %s%s
101*constraints2
4.1 2default:default2&
Global Iteration 02default:defaultZ18-101h px� 
�
Intermediate Timing Summary %s164*route2J
6| WNS=0.558  | TNS=0.000  | WHS=N/A    | THS=N/A    |
2default:defaultZ35-416h px� 
H
3Phase 4.1 Global Iteration 0 | Checksum: 19af89cd7
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:18 ; elapsed = 00:00:51 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
F
1Phase 4 Rip-up And Reroute | Checksum: 19af89cd7
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:18 ; elapsed = 00:00:51 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
|

Phase %s%s
101*constraints2
5 2default:default2/
Delay and Skew Optimization2default:defaultZ18-101h px� 
p

Phase %s%s
101*constraints2
5.1 2default:default2!
Delay CleanUp2default:defaultZ18-101h px� 
C
.Phase 5.1 Delay CleanUp | Checksum: 19af89cd7
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:19 ; elapsed = 00:00:51 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
z

Phase %s%s
101*constraints2
5.2 2default:default2+
Clock Skew Optimization2default:defaultZ18-101h px� 
M
8Phase 5.2 Clock Skew Optimization | Checksum: 19af89cd7
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:19 ; elapsed = 00:00:51 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
O
:Phase 5 Delay and Skew Optimization | Checksum: 19af89cd7
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:19 ; elapsed = 00:00:51 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
n

Phase %s%s
101*constraints2
6 2default:default2!
Post Hold Fix2default:defaultZ18-101h px� 
p

Phase %s%s
101*constraints2
6.1 2default:default2!
Hold Fix Iter2default:defaultZ18-101h px� 
r

Phase %s%s
101*constraints2
6.1.1 2default:default2!
Update Timing2default:defaultZ18-101h px� 
E
0Phase 6.1.1 Update Timing | Checksum: 186a38b87
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:21 ; elapsed = 00:00:52 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
�
Intermediate Timing Summary %s164*route2J
6| WNS=0.574  | TNS=0.000  | WHS=0.044  | THS=0.000  |
2default:defaultZ35-416h px� 
C
.Phase 6.1 Hold Fix Iter | Checksum: 218a41eb9
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:21 ; elapsed = 00:00:52 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
A
,Phase 6 Post Hold Fix | Checksum: 218a41eb9
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:21 ; elapsed = 00:00:52 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
o

Phase %s%s
101*constraints2
7 2default:default2"
Route finalize2default:defaultZ18-101h px� 
B
-Phase 7 Route finalize | Checksum: 1ea00125f
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:21 ; elapsed = 00:00:53 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
v

Phase %s%s
101*constraints2
8 2default:default2)
Verifying routed nets2default:defaultZ18-101h px� 
I
4Phase 8 Verifying routed nets | Checksum: 1ea00125f
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:22 ; elapsed = 00:00:53 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
r

Phase %s%s
101*constraints2
9 2default:default2%
Depositing Routes2default:defaultZ18-101h px� 
E
0Phase 9 Depositing Routes | Checksum: 1c5c0fdb6
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:25 ; elapsed = 00:00:56 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
t

Phase %s%s
101*constraints2
10 2default:default2&
Post Router Timing2default:defaultZ18-101h px� 
�
Estimated Timing Summary %s
57*route2J
6| WNS=0.574  | TNS=0.000  | WHS=0.044  | THS=0.000  |
2default:defaultZ35-57h px� 
�
�The final timing numbers are based on the router estimated timing analysis. For a complete and accurate timing signoff, please run report_timing_summary.
127*routeZ35-327h px� 
G
2Phase 10 Post Router Timing | Checksum: 1c5c0fdb6
*commonh px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:25 ; elapsed = 00:00:56 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
@
Router Completed Successfully
2*	routeflowZ35-16h px� 
�

%s
*constraints2q
]Time (s): cpu = 00:01:25 ; elapsed = 00:00:56 . Memory (MB): peak = 2173.793 ; gain = 203.9302default:defaulth px� 
Z
Releasing license: %s
83*common2"
Implementation2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
1122default:default2
792default:default2
12default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
route_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
route_design: 2default:default2
00:01:292default:default2
00:00:592default:default2
2173.7932default:default2
203.9302default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0112default:default2
2173.7932default:default2
0.0002default:defaultZ17-268h px� 
H
&Writing timing data to binary archive.266*timingZ38-480h px� 
D
Writing placer database...
1603*designutilsZ20-1893h px� 
=
Writing XDEF routing.
211*designutilsZ20-211h px� 
J
#Writing XDEF routing logical nets.
209*designutilsZ20-209h px� 
J
#Writing XDEF routing special nets.
210*designutilsZ20-210h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2)
Write XDEF Complete: 2default:default2
00:00:082default:default2
00:00:032default:default2
2173.7932default:default2
0.0002default:defaultZ17-268h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2w
cC:/kintex_dsp_lite/projects/Kintex_DSP_Lite/Kintex_DSP_Lite.runs/impl_1/design_1_wrapper_routed.dcp2default:defaultZ17-1381h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2&
write_checkpoint: 2default:default2
00:00:112default:default2
00:00:072default:default2
2173.7932default:default2
0.0002default:defaultZ17-268h px� 
�
%s4*runtcl2�
�Executing : report_drc -file design_1_wrapper_drc_routed.rpt -pb design_1_wrapper_drc_routed.pb -rpx design_1_wrapper_drc_routed.rpx
2default:defaulth px� 
�
Command: %s
53*	vivadotcl2�
xreport_drc -file design_1_wrapper_drc_routed.rpt -pb design_1_wrapper_drc_routed.pb -rpx design_1_wrapper_drc_routed.rpx2default:defaultZ4-113h px� 
>
IP Catalog is up to date.1232*coregenZ19-1839h px� 
P
Running DRC with %s threads
24*drc2
22default:defaultZ23-27h px� 
�
#The results of DRC are in file %s.
168*coretcl2�
gC:/kintex_dsp_lite/projects/Kintex_DSP_Lite/Kintex_DSP_Lite.runs/impl_1/design_1_wrapper_drc_routed.rptgC:/kintex_dsp_lite/projects/Kintex_DSP_Lite/Kintex_DSP_Lite.runs/impl_1/design_1_wrapper_drc_routed.rpt2default:default8Z2-168h px� 
\
%s completed successfully
29*	vivadotcl2

report_drc2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2 
report_drc: 2default:default2
00:00:122default:default2
00:00:062default:default2
2185.1952default:default2
11.4022default:defaultZ17-268h px� 
�
%s4*runtcl2�
�Executing : report_methodology -file design_1_wrapper_methodology_drc_routed.rpt -pb design_1_wrapper_methodology_drc_routed.pb -rpx design_1_wrapper_methodology_drc_routed.rpx
2default:defaulth px� 
�
Command: %s
53*	vivadotcl2�
�report_methodology -file design_1_wrapper_methodology_drc_routed.rpt -pb design_1_wrapper_methodology_drc_routed.pb -rpx design_1_wrapper_methodology_drc_routed.rpx2default:defaultZ4-113h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
Y
$Running Methodology with %s threads
74*drc2
22default:defaultZ23-133h px� 
�
2The results of Report Methodology are in file %s.
450*coretcl2�
sC:/kintex_dsp_lite/projects/Kintex_DSP_Lite/Kintex_DSP_Lite.runs/impl_1/design_1_wrapper_methodology_drc_routed.rptsC:/kintex_dsp_lite/projects/Kintex_DSP_Lite/Kintex_DSP_Lite.runs/impl_1/design_1_wrapper_methodology_drc_routed.rpt2default:default8Z2-1520h px� 
d
%s completed successfully
29*	vivadotcl2&
report_methodology2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2(
report_methodology: 2default:default2
00:00:192default:default2
00:00:112default:default2
2254.9222default:default2
69.7272default:defaultZ17-268h px� 
�
%s4*runtcl2�
�Executing : report_power -file design_1_wrapper_power_routed.rpt -pb design_1_wrapper_power_summary_routed.pb -rpx design_1_wrapper_power_routed.rpx
2default:defaulth px� 
�
Command: %s
53*	vivadotcl2�
�report_power -file design_1_wrapper_power_routed.rpt -pb design_1_wrapper_power_summary_routed.pb -rpx design_1_wrapper_power_routed.rpx2default:defaultZ4-113h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
K
,Running Vector-less Activity Propagation...
51*powerZ33-51h px� 
P
3
Finished Running Vector-less Activity Propagation
1*powerZ33-1h px� 
�
�Detected over-assertion of set/reset/preset/clear net with high fanouts, power estimation might not be accurate. Please run Tool - Power Constraint Wizard to set proper switching activities for control signals.282*powerZ33-332h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
1242default:default2
802default:default2
12default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
report_power2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
report_power: 2default:default2
00:00:152default:default2
00:00:092default:default2
2256.1642default:default2
1.2422default:defaultZ17-268h px� 
�
%s4*runtcl2�
mExecuting : report_route_status -file design_1_wrapper_route_status.rpt -pb design_1_wrapper_route_status.pb
2default:defaulth px� 
�
%s4*runtcl2�
�Executing : report_timing_summary -max_paths 10 -file design_1_wrapper_timing_summary_routed.rpt -pb design_1_wrapper_timing_summary_routed.pb -rpx design_1_wrapper_timing_summary_routed.rpx -warn_on_violation 
2default:defaulth px� 
r
UpdateTimingParams:%s.
91*timing29
% Speed grade: -2, Delay Type: min_max2default:defaultZ38-91h px� 
|
CMultithreading enabled for timing update using a maximum of %s CPUs155*timing2
22default:defaultZ38-191h px� 
�
}There are set_bus_skew constraint(s) in this design. Please run report_bus_skew to ensure that bus skew requirements are met.223*timingZ38-436h px� 
�
%s4*runtcl2m
YExecuting : report_incremental_reuse -file design_1_wrapper_incremental_reuse_routed.rpt
2default:defaulth px� 
g
BIncremental flow is disabled. No incremental reuse Info to report.423*	vivadotclZ4-1062h px� 
�
%s4*runtcl2m
YExecuting : report_clock_utilization -file design_1_wrapper_clock_utilization_routed.rpt
2default:defaulth px� 
�
%s4*runtcl2�
�Executing : report_bus_skew -warn_on_violation -file design_1_wrapper_bus_skew_routed.rpt -pb design_1_wrapper_bus_skew_routed.pb -rpx design_1_wrapper_bus_skew_routed.rpx
2default:defaulth px� 
r
UpdateTimingParams:%s.
91*timing29
% Speed grade: -2, Delay Type: min_max2default:defaultZ38-91h px� 
|
CMultithreading enabled for timing update using a maximum of %s CPUs155*timing2
22default:defaultZ38-191h px� 


End Record